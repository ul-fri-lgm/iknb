'use strict';

angular
    .module('sloDialectsApp')
    .factory('dialects', [function() {

        var dialectGroups = {
            "gorenjska_narecna_skupina": {
                name: "Gorenjska narečna skupina",
                dialects: {
                    "gorenjsko_narecje": {
                        name: "Gorenjsko narečje",
                        subdialects: {
                            "_vzhodnogorenjsko_podnarecje": { name: "Vzhodnogorenjsko podnarečje" },
                        },
                    },
                    "selsko_narecje": { name: "Selško narečje" },
                }
            },
            "dolenjska_narecna_skupina": {
                name: "Dolenjska narečna skupina",
                dialects: {
                    "dolenjsko_narecje": {
                        name: "Dolenjsko narečje",
                        subdialects: {
                            "_vzhodnodolenjsko_podnarecje": { name: "Vzhodnodolenjsko podnarečje" }
                        }
                    },
                    "severnobelokranjsko_narecje": { name: "Severnobelokranjsko narečje" },
                    "juznobelokranjsko_narecje": { name: "Južnobelokranjsko narečje" },
                    "kostelsko_narecje": { name: "Kostelsko narečje" },
                    "mesani_kocevski_govori": { name: "Mešani kočevski govori" },
                }
            },
            "stajerska_narecna_skupina":{
                name: "Štajerska narečna skupina",
                dialects: {
                    "srednjesavinjsko_narecje": { name: "Srednjesavinsko narečje" },
                    "zgornjesavinjsko_narecje": {
                        name: "Zgornjesavinjsko narečje",
                        subdialects: {
                            "_solcavsko_podnarecje": { name: "Solčavsko podnarečje" },
                        }
                    },
                    "srednjestajersko_narecje": { name: "Srednještajersko narečje" },
                    "juznopohorsko_narecje": {
                        name: "Južnopohorsko narečje",
                        subdialects: {
                            "_kozjasko_podnarecje": { name: "Kozjaško podnarečje" }
                        }
                    },
                    "kozjansko-bizeljsko_narecje": { name: "Kozjansko-bizeljsko narečje" },
                    "posavsko_narecje": {
                        name: "Posavsko narečje",
                        subdialects: {
                            "_zagorsko_trboveljsko_podnarecje": { name: "Zagorsko trboveljsko podnarečje" },
                            "_lasko_podnarecje": { name: "Laško podnarečje" },
                            "_sevnisko-krsko_podnarecje": { name: "Sevniško-krško podnarečje" },
                        }
                    }, 
                }
            },
            "panonska_narecna_skupina": {
                name: "Panonska narečna skupina",
                dialects: {
                    "prekmursko_narecje": { name: "Prekmursko narečje" },
                    "slovenskogorisko_narecje": { name: "Slovenskogoriško narečje" },
                    "prlesko_narecje": { name: "Prleško narečje" },
                    "halosko_narecje": { name: "Haloško narečje" }, 
                }
            },
            "koroska_narecna_skupina": {
                name: "Koroška narečna skupina",
                dialects: {
                    "severnopohorsko-remsnisko_narecje": { name: "Severnopohorsko-remšniško narečje" },
                    "mezisko_narecje": { name: "Mežiško narečje" },
                    "podjunsko_narecje": { name: "Podjunsko narečje" },
                    "obirsko_narecje": { name: "Obirsko narečje" },
                    "rozansko_narecje": { name: "Rožansko narečje" },
                    "ziljsko_narecje":{
                        name: "Ziljsko narečje",
                        subdialects: {
                            "_kranjskogorsko_podnarecje": { name: "Kranjskogorsko podnarečje" },
                        }
                    }, 
                }
            },
            "primorska_narecna_skupina": {
                name: "Primorska narečna skupina",
                dialects: {
                    "rezijansko_narecje": { name: "Rezijansko narečje" },
                    "obsosko_narecje": { name: "Obsoško narečje" },
                    "tersko_narecje": { name: "Tersko narečje" },
                    "nadisko_narecje": { name: "Nadiško narečje" },
                    "brisko_narecje": { name: "Briško narečje" },
                    "krasko_narecje": {
                        name: "Kraško narečje",
                        subdialects: {
                            "_banjsko_podnarecje": { name: "Banjško podnarečje" },
                        }
                    },
                    "istrsko_narecje": {
                        name: "Istrsko narečje",
                        subdialects: {
                            "_rizansko_podnarecje": { name: "Rižansko podnarečje" },
                            "_savrinsko_podnarecje": { name: "Šavrinsko podnarečje" },
                        }
                    },
                    "notranjsko_narecje": { name: "Notranjsko narečje" },
                    "cisko_narecje": { name: "Čiško narečje" },
                }
            },
            "rovtarska_narecna_skupina":{
                name: "Rovtarska narečna skupina",
                dialects: {
                    "tolminsko_narecje": {
                        name: "Tolminsko narečje",
                        subdialects: {
                            "_basko_podnarecje": { name: "Baško podnarečje" },
                        },
                    },
                    "cerkljansko_narecje": { name: "Cerkljansko narečje" },
                    "poljansko_narecje": { name: "Poljansko narečje" },
                    "skofjelosko_narecje": { name: "Škofjeloško narečje" },
                    "crnovrsko_narecje": { name: "Črnovrško narečje" },
                    "horjulsko_narecje": { name: "Horjulsko narečje" },
                }
            }
        }

        return dialectGroups;

    }]);