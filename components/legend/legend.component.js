'use strict';

angular
  .module('sloDialectsApp')
  .component('dialectLegend', {
    templateUrl: 'components/legend/legend.template.html',
    controller: ['geojsonService', '$scope', '$rootScope', 'dialects', function LegendController(geojsonService, $scope, $rootScope, dialects) {

      var vm = this;
      vm.dialectGroups = dialects;
      vm.mouseEnter = mouseEnter;
      vm.mouseLeave = mouseLeave;

      function mouseEnter(id, type){
        var hoverWeight = 5;
        if(id.indexOf('narecna_skupina') > -1){
          $rootScope.layersArr[id].bringToFront();
        }
        $rootScope.layersArr[id].setStyle({
          weight: hoverWeight,
          color: "#b23636"
        });
      }

      function mouseLeave(elem, type){
        var id = elem.id;
        var resetWeight = 1;
				if(id.indexOf('narecna_skupina') > -1){
          $rootScope.layersArr[id].bringToBack();
					resetWeight = 3;
        }
        $rootScope.layersArr[id].setStyle({
          weight: resetWeight,
          color: "#000000"
        });
        
      }

    }]
  });