'use strict';

angular
    .module('sloDialectsApp')
    .component('dialectComponent', {
        templateUrl: 'components/dialect/dialect.template.html',
        bindings: {
            resolve: '<',
            close: '&',
            dismiss: '&',
            modalInstance: '<'
        },
        controller: ['$scope', '$rootScope', 'Config', function dialectController($scope, $rootScope, Config) {
            var vm = this;

            vm.$onInit = function () {
                // injected resolve
                vm.dialectData = vm.resolve.dialectData;
		vm.dialectData.audio = Config.APP_PATH + vm.dialectData.audio;
                vm.analysisData = prepareAnalysisData(vm.resolve.analysisData);

                // prepare print view
                $rootScope.printData = $rootScope.printData || {};
                $rootScope.printData.dialectData = vm.dialectData;
                $rootScope.printData.analysisData = vm.analysisData;

                // injected modal result
                vm.modalInstance.result.then(function(){
                    if(vm.wavesurfer){
                        vm.wavesurfer.destroy(); // close button clicked (success)
                    }
                }, function(){
                    if(vm.wavesurfer){
                        vm.wavesurfer.destroy(); // ESC key or backdrop click (fail)
                    }
                });
            };

            function prepareAnalysisData(analysis){
                var _analysisData = [];
                analysis.forEach(function(a){
                    _analysisData[a.section] = _analysisData[a.section] || [];
                    _analysisData[a.section].push({
                        maintext: a.maintext,
                        examples: a.examples
                    });
                });
                return _analysisData;
            }

            vm.ok = function(){
                vm.close();
            }

            // wafesurfer //
            var activeUrl = null;

            vm.paused = true;

            $scope.$on('wavesurferInit', function (e, wavesurfer) {

                vm.wavesurfer = vm.wavesurfer || wavesurfer;

                /*
                wavesurfer.on('loading', function (percents) {
                    document.getElementById('progress').value = percents;
                    $scope.$apply();
                  });
              
                wavesurfer.on('ready', function (percents) {
                document.getElementById('progress').style.display = 'none';
                $scope.$apply();
                });
                */

                vm.wavesurfer.on('play', function () {
                    vm.paused = false;
                });

                vm.wavesurfer.on('pause', function () {
                    vm.paused = true;
                });

                vm.wavesurfer.on('finish', function () {
                    vm.paused = true;
                    vm.wavesurfer.seekTo(0);
                    $scope.$apply();
                });
            });

            vm.play = function (url) {
                if (!vm.wavesurfer) {
                    return;
                }

                activeUrl = url;

                vm.wavesurfer.once('ready', function () {
                    vm.wavesurfer.play();
                    $scope.$apply();
                });

                vm.wavesurfer.load(activeUrl);
            };

            vm.isPlaying = function (url) {
                return url == activeUrl;
            };

        }]
    });
