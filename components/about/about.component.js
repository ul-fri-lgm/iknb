'use strict';

angular
    .module('sloDialectsApp')
    .component('aboutComponent', {
        templateUrl: 'components/about/about.template.html',
        bindings: {
            resolve: '<',
            close: '&',
            dismiss: '&',
            modalInstance: '<'
        },
        controller: [function aboutController() {
            var vm = this;

            vm.$onInit = function () {
                // injected result
                vm.modalInstance.result.then(function(){
                    
                }, function(){
                    
                });
            };

        }]
    });