'use strict';

angular
    .module('sloDialectsApp')
    .component('print', {
        templateUrl: 'components/print/print.template.html',
        controller: ['$scope', '$rootScope', function printController($scope, $rootScope) {

            var vm = this;

            $scope.$watch(
                function() { return ($rootScope.printData && $rootScope.printData.dialectData) ? $rootScope.printData.dialectData : '' },
                function(dialectData) {
                    if(dialectData){
                        vm.dialectData = dialectData;
                    }
                }
            );

            $scope.$watch(
                function() { return ($rootScope.printData && $rootScope.printData.analysisData) ? $rootScope.printData.analysisData : '' },
                function(analysisData) {
                    if(analysisData){
                        vm.analysisData = analysisData;
                    }
                }
            );

        }]
    });