'use strict';

angular
	.module('sloDialectsAdmin')
	.component('user', {
		templateUrl: 'components/user/user.template.html',
		controller: ['$http', '$location', '$timeout', function UsersController($http, $location, $timeout) {

			var vm = this;
			vm.go = go;

			

			vm.addUser = function(){

				vm.success = "";
				vm.error = "";
				var is_admin = vm.is_admin ? 1 : 0;
				$http.post('../api/users', {name: vm.name, username: vm.username, password: vm.password, password_repeat: vm.password_repeat, is_admin: is_admin}).then(function (result) {
					var data = result.data;
					if(data.success){
						vm.success = data.message;

						$timeout(function(){
							go('/users');
						}, 3000);

					} else {
						vm.error = data.message;
					}
				}, function (error) {
					console.log('Error creating user', error);
					vm.error = error;
				});
			}

			function go(path) {
				$location.path(path);
			}

		}]
	});