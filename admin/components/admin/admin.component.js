/*
 * Edited by:     Nermin Jukan, 63150367
 * Date:          21. 05. 2018
 * Modifications: Edited the delete function so that it shows the Name property and no the ID.
 */

'use strict';

angular
	.module('sloDialectsAdmin')
	.component('admin', {
		templateUrl: 'components/admin/admin.template.html',
		controller: ['$http', '$location', 'dialectSelect', function AdminController($http, $location, dialectSelect) {

			var vm = this;
			vm.go = go;
            vm.selectItems = dialectSelect;
			vm.createNew = createNew;
			getDialects();


			function getDialects(){
				$http.get('../api/dialects').then(function (result) {
					vm.dialects = result.data;
				}, function (error) {
					console.log('Error getting dialects');
				});
			}


			vm.delete = function (id, name) {

                if (confirm("Ali ste prepričani, da želite izbrisati vnos " + name + "?")) {
					$http.delete('../api/dialects/'+id).then(function(result){
						//console.log(result.data);
						getDialects();
					}, function(error){
						console.log('Error deleting entry:', error);
					});
				}
				
			};

			function createNew() {

				$http.post('../api/dialects').then(function (response) {
					go('/new/' + response.data.id);
				}, function (error) {
					alert('Prišlo je do napake pri dodajanju novega vnosa:', error);
				});
			}

			function go(path) {
				$location.path(path);
			}

		}]
	});