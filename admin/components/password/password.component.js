'use strict';

angular
	.module('sloDialectsAdmin')
	.component('password', {
		templateUrl: 'components/password/password.template.html',
		controller: ['$http', '$location', '$timeout', function PasswordController($http, $location, $timeout) {

			var vm = this;
			vm.go = go;

			vm.username = "";

			vm.changePassword = function(){

				vm.error = "";
				
				// TODO
				$http.put('../api/changePassword', {old_password: vm.old_password, new_password: vm.new_password, new_password_repeat: vm.new_password_repeat}).then(function (result) {
					if(result.data.success){
						vm.success = result.data.message;
						$timeout(function(){
							go('/login');
						}, 3000);
					} else {
						vm.error = result.data.message;
					}
				}, function (error, x) {
					vm.error = "Prišlo je do napake pri zahtevi za menjavo gesla.";
				});
				
			}

			function go(path) {
				$location.path(path);
			}

		}]
	});