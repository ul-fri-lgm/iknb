'use strict';

angular
  .module('sloDialectsAdmin', [
    'ui.bootstrap',
    'ngRoute',

    'ngCookies'
  ])
  .constant('Config', {
      // If app not in server root, add relative path, otherwise leave empty string. Define also in api/config.php and app.module.js!
      APP_PATH: '/IKNB'
  });

