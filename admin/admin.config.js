'use strict';

angular
	.module('sloDialectsAdmin')
	.config(function($routeProvider) {
		$routeProvider
			.when("/", {
				template: "<admin></admin>" // list items
			})
			.when("/edit/:id", {
				template: "<edit></edit>" // edit item
			})
            .when("/new/:id", {
                template: "<new></new>" // edit item
            })
			.when('/login', {
				template: '<auth></auth>'
			})
			.when('/users', {
				template: '<users></users>'
			})
			.when('/user', {
				template: '<user></user>'
			})
			.when('/password', {
				template: '<password></password>'
			})
			.otherwise('/login');
		})

		.run(['$rootScope', '$location', '$cookieStore', '$http',
			function ($rootScope, $location, $cookieStore, $http) {
				// keep user logged in after page refresh
				$rootScope.globals = $cookieStore.get('globals') || {};
				if ($rootScope.globals.currentUser) {
					$http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
				}
		 
				$rootScope.$on('$locationChangeStart', function (event, next, current) {
					// redirect to login page if not logged in
					if ($location.path() !== '/login' && !$rootScope.globals.currentUser) {
						$location.path('/login');
					}
				});
			}]);