#O spletni aplikaciji

##Avtor
Avtor spletne aplikacije je Ivan Lovrić, študent Fakultete za računalništvo in informatiko Univerze v Ljubljani. Aplikacijo je izdelal v okviru diplomske naloge v letu 2018 pod mentorstvom viš. pred. dr. Alenke Kavčič (UL FRI) in somentorstvom prof. dr. Vere Smole (UL FF).
Izdelano spletno aplikacijo je z dodatnimi funkcionalnostmi nadgradil Nermin Jukan, študent Fakultete za računalništvo in informatiko Univerze v Ljubljani.

##Aplikacija
Interaktivna karta slovenskih narečnih besedil / Interactive map of Slovenian dialectal texts
Copyright © 2018 Ivan Lovrić (ivan@lovric.si)

Ta program spada med prosto programje; lahko ga razširjate in/ali spreminjate pod pogoji Splošnega dovoljenja GNU (GNU General Public License), različice 3, kot ga je objavila ustanova Free Software Foundation.
Ta program se razširja v upanju, da bo uporaben, vendar BREZ VSAKRŠNEGA JAMSTVA; tudi brez posredne zagotovitve CENOVNE VREDNOSTI ali PRIMERNOSTI ZA DOLOČEN NAMEN. Za podrobnosti glejte besedilo GNU General Public License.
Skupaj s tem programom bi morali prejeti izvod Splošnega dovoljenja GNU (GNU General Public License). Podrobnosti licence so dostopne tudi na spletni strani http://www.gnu.org/licenses. 

Izvorna koda aplikacije je dosegljiva v repozitoriju Bitbucket: https://bitbucket.org/ul-fri-lgm/iknb.

##Vsebina (posnetki, transkripcije, poknjižitve in analize)
Vsebino za Interaktivno karto slovenskih narečnih besedil (IKNB) za temo Stare kmečke hiše so zbrali in pripravili študenti Oddelka za slovenistiko Filozofske fakultete Univerze v Ljubljani. Zvočne posnetke, transkripcije in poknjižitve besedil so prispevali študentje izbirnega predmeta Slovenska narečja pod vodstvom prof. dr. Vere Smole in asist. dr. Mojce Kumin Horvat, analize pa študentje izbirnega predmeta Poglavja iz zgodovine slovenskega glasoslovja in seminarja pri predmetu Slovenska dialektologija pod vodstvom prof. dr. Vere Smole. 
Vsem študentom, preteklim in bodočim, in njihovim informatorjem se zahvaljujemo za sodelovanje in njihov prispevek.

##Karta slovenskih narečij 
Interaktivna karta slovenskih narečnih besedil (IKNB) temelji na Karti slovenskih narečij. Karto sta priredila Tine Logar in Jakob Rigler (1983) na osnovi Dialektološke karte slovenskega jezika Frana Ramovša (1931), novejših raziskav in gradiva Inštituta za slovenski jezik ZRC SAZU, jo dopolnili Vera Smole in Jožica Škofic (2011) in nato še sodelavci Dialektološke sekcije ISJFR ZRC SAZU (2016).

© Inštitut za slovenski jezik Frana Ramovša ZRC SAZU, Geografski inštitut Antona Melika ZRC SAZU ter Inštitut za antropološke in prostorske študije ZRC SAZU, 2016 

##Pisava ZRCola
Besedilo je bilo pripravljeno z vnašalnim sistemom ZRCola (http://zrcola.zrc-sazu.si), ki ga je na Znanstvenoraziskovalnem centru SAZU v Ljubljani (http://www.zrc-sazu.si) razvil Peter Weiss.

Zadnja sprememba: julij 2018, Nermin Jukan
