Projekt IKNB (Interaktivna karta slovenskih narečnih besedil / Interactive map of Slovenian dialectal texts)

Interaktivna spletna aplikacija za slovenska narečna besedila, diploma Ivan Lovrić, UL FRI, 2018
nadgradnja aplikacije Nermin Jukan, UL FRI, 2018

Direktorij projekta vsebuje:
- to datoteko readme.txt
- kodo aplikacije v direktoriju iknb
- začetno podatkovno bazo v datoteki dialectsdb.sql
- navodila za namestitev aplikacije Namestitev.txt
