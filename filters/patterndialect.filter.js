/*
* Edited by:     Nermin Jukan, 63150367
* Date:          16. 05. 2018
* Modifications: Added a new filter for "cisko_narecje_2" on lines 78-85.
*
* */

'use strict';

angular
    .module('sloDialectsApp')
    .filter('patternDialect', function() {

        var stripePatterns = {
            "severnopohorsko-remsnisko_narecje": {
                weight: 8,
                spaceWeight: 4,
                height: 12,
                color: '#fea3e9',
                spaceColor: '#b0ef91',
                angle: 45,
                spaceOpacity: 1
            },
            "mezisko_narecje": {
                weight: 8,
                spaceWeight: 4,
                height: 12,
                color: '#fea3e9',
                spaceColor: '#b0ef91',
                angle: -45,
                spaceOpacity: 1
            },
            "obirsko_narecje": {
                weight: 8,
                spaceWeight: 4,
                height: 12,
                color: '#ff83e2',
                spaceColor: '#d2a0fc',
                angle: 90,
                spaceOpacity: 1
            },
            "rezijansko_narecje": {
                weight: 7,
                spaceWeight: 5,
                height: 12,
                color: '#f0a086',
                spaceColor: '#ffa4e5',
                angle: 90,
                spaceOpacity: 1
            },
            "brisko_narecje": {
                weight: 6,
                spaceWeight: 6,
                height: 12,
                color: '#fd9059',
                spaceColor: '#ffc6ac',
                angle: 90,
                spaceOpacity: 1
            },
            "notranjsko_narecje": {
                weight: 6,
                spaceWeight: 6,
                height: 12,
                color: '#ff9900',
                spaceColor: '#c77e5a',
                angle: -45,
                spaceOpacity: 1
            },
            "cisko_narecje": {
                weight: 4,
                spaceWeight: 8,
                height: 12,
                color: '#0000aa',
                spaceColor: '#ff9900',
                angle: 0,
                spaceOpacity: 1
            },
            "cisko_narecje_2": {
                weight: 4,
                spaceWeight: 10,
                height: 12,
                color: '#aeaeae',
                angle: 45,
                spaceOpacity: 0.2
            },
            "selsko_narecje": {
                weight: 5,
                spaceWeight: 7,
                height: 12,
                color: '#d2a0fc',
                spaceColor: '#8d93f8',
                angle: 0,
                spaceOpacity: 1
            },
            "severnobelokranjsko_narecje": {
                
                weight: 8,
                spaceWeight: 4,
                height: 12,
                color: '#ff9900',
                spaceColor: '#aeaeae',
                angle: 90,
                spaceOpacity: 1
                /*
                weight: 5,
                spaceWeight: 7,
                height: 12,
                color: '#c77e5a',
                spaceColor: '#aeaeae',
                angle: 0,
                spaceOpacity: 1
                */
            },
            "juznobelokranjsko_narecje": {
                /*
                weight: 4,
                spaceWeight: 4,
                height: 8,
                color: '#c77e5a',
                spaceColor: '#aeaeae',
                angle: 90,
                spaceOpacity: 1
                */
                weight: 5,
                spaceWeight: 7,
                height: 12,
                color: '#ff9900',
                spaceColor: '#aeaeae',
                angle: 0,
                spaceOpacity: 1
            },
            "slovenskogorisko_narecje": {
                weight: 6,
                spaceWeight: 6,
                height: 12,
                color: '#ffff7b',
                spaceColor: '#b0ef91',
                angle: 0,
                spaceOpacity: 1
            },
            "prlesko_narecje": {
                weight: 6,
                spaceWeight: 6,
                height: 12,
                color: '#ffff7b',
                spaceColor: '#efef5a',
                angle: -45,
                spaceOpacity: 1
            },
            "halosko_narecje": {
                weight: 8,
                spaceWeight: 4,
                height: 12,
                color: '#ffff7b',
                spaceColor: '#7ec75a',
                angle: 90,
                spaceOpacity: 1
            },
            "kostelsko_narecje": {
                weight: 8,
                spaceWeight: 4,
                height: 12,
                color: '#ff9900',
                spaceColor: '#aeaeae',
                angle: -45,
                spaceOpacity: 1
            }
        }

        var circlePatterns = {
            "_vzhodnogorenjsko_podnarecje": {
                fill: true,
                fillOpacity: 1,
                x: 10,
                y: 10,
                radius: 4,
                stroke: false,
                color: '#7ec75a'
            },
            "_vzhodnodolenjsko_podnarecje": {
                fill: true,
                fillOpacity: 1,
                x: 10,
                y: 10,
                radius: 4,
                stroke: false,
                color: '#7ec75a'
            },
            "mesani_kocevski_govori": {
                fill: true,
                fillOpacity: 1,
                x: 10,
                y: 10,
                radius: 5,
                stroke: false,
                color: '#ff9900'
            },
            "_solcavsko_podnarecje": {
                fill: true,
                fillOpacity: 1,
                x: 10,
                y: 10,
                radius: 4,
                stroke: false,
                color: '#ffcfee'
            },
            "_kozjasko_podnarecje": {
                fill: true,
                fillOpacity: 1,
                x: 10,
                y: 10,
                radius: 6,
                stroke: false,
                color: '#7ec75a'
            },
            "_zagorsko_trboveljsko_podnarecje": {
                fill: true,
                fillOpacity: 1,
                x: 10,
                y: 10,
                radius: 5,
                stroke: false,
                color: '#7ec75a'
            },
            "_lasko_podnarecje": {
                fill: true,
                fillOpacity: 1,
                x: 10,
                y: 10,
                radius: 9,
                stroke: false,
                color: '#7ec75a'
            },
            "_sevnisko-krsko_podnarecje": {
                fill: true,
                fillOpacity: 1,
                x: 10,
                y: 10,
                radius: 7,
                stroke: false,
                color: '#7ec75a'
            },
            "_kranjskogorsko_podnarecje": {
                fill: true,
                fillOpacity: 1,
                x: 10,
                y: 10,
                radius: 7,
                stroke: false,
                color: '#d2a0fc'
            },
            "_banjsko_podnarecje": {
                fill: true,
                fillOpacity: 1,
                x: 10,
                y: 10,
                radius: 3,
                stroke: false,
                color: '#a4cdff'
            },
            "_rizansko_podnarecje": {
                fill: true,
                fillOpacity: 1,
                x: 10,
                y: 10,
                radius: 3,
                stroke: false,
                color: '#ffc6ac'
            },
            "_savrinsko_podnarecje": {
                fill: true,
                fillOpacity: 1,
                x: 10,
                y: 10,
                radius: 6,
                stroke: false,
                color: '#ffc6ac'
            },
            "_basko_podnarecje": {
                fill: true,
                fillOpacity: 1,
                x: 10,
                y: 10,
                radius: 3,
                stroke: false,
                color: '#d2a0fc'
            }

            
        }

        return function(input) {
            if( stripePatterns[input] ){
                return {
                    type:'stripes',
                    config: stripePatterns[input]
                }
            } else if( circlePatterns[input] ){
                return {
                    type:'circles',
                    config: circlePatterns[input]
                }
            } else {
                return false;
            }
        };
    });