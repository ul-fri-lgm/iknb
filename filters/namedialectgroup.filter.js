'use strict';

angular
    .module('sloDialectsApp')
    .filter('nameDialectGroup', function() {

        return function(input) {

            switch(input){
                case "panonska_narecna_skupina":
                case "halosko_narecje":
                case "prlesko_narecje":
                case "prekmursko_narecje":
                case "slovenskogorisko_narecje":
                    return "panonska narečna skupina";

                case "stajerska_narecna_skupina":
                case "_sevnisko-krsko_podnarecje":
                case "_zagorsko_trboveljsko_podnarecje":
                case "_lasko_podnarecje":
                case "posavsko_narecje":
                case "kozjansko-bizeljsko_narecje":
                case "srednjesavinjsko_narecje":
                case "_solcavsko_podnarecje":
                case "zgornjesavinjsko_narecje":
                case "srednjestajersko_narecje":
                case "juznopohorsko_narecje":
                case "_kozjasko_podnarecje":
                    return "štajerska narečna skupina";

                case "dolenjska_narecna_skupina":
                case "_vzhodnodolenjsko_podnarecje":
                case "dolenjsko_narecje":
                case "kostelsko_narecje":
                case "mesani_kocevski_govori":
                case "juznobelokranjsko_narecje":
                case "severnobelokranjsko_narecje":
                    return "dolenjska narečna skupina";

                case "primorska_narecna_skupina":
                case "cisko_narecje":
                case "notranjsko_narecje":
                case "_savrinsko_podnarecje":
                case "_rizansko_podnarecje":
                case "istrsko_narecje":
                case "krasko_narecje":
                case "obsosko_narecje":
                case "rezijansko_narecje":
                case "tersko_narecje":
                case "nadisko_narecje":
                case "brisko_narecje":
                case "_banjsko_podnarecje":
                    return "primorska narečna skupina";

                case "rovtarska_narecna_skupina":
                case "tolminsko_narecje":
                case "_basko_podnarecje":
                case "skofjelosko_narecje":
                case "horjulsko_narecje":
                case "crnovrsko_narecje":
                case "cerkljansko_narecje":
                case "poljansko_narecje":
                    return "rovtarska narečna skupina";

                case "gorenjska_narecna_skupina":
                case "gorenjsko_narecje":
                case "selsko_narecje":
                case "_vzhodnogorenjsko_podnarecje":
                    return "gorenjska narečna skupina";
                
                case "koroska_narecna_skupina":
                case "ziljsko_narecje":
                case "_kranjskogorsko_podnarecje":
                case "rozansko_narecje":
                case "podjunsko_narecje":
                case "obirsko_narecje":
                case "mezisko_narecje":
                case "severnopohorsko-remsnisko_narecje":
                    return "koroška narečna skupina";
                
                default:
                    return "";
            }

        };
    });