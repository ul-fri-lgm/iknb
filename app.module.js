'use strict';

// define the 'sloDialectsApp' module
angular
  .module('sloDialectsApp', [
    'ui.bootstrap',
    'ui-leaflet'
  ])
  .constant('Config', {
    // If app not in server root, add relative path, otherwise leave empty string. Define also in api/config.php and admin/admin.module.js!
    APP_PATH: '/IKNB'
  });
