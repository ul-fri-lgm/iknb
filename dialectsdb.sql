-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 04, 2018 at 01:22 PM
-- Server version: 5.7.20-0ubuntu0.16.04.1
-- PHP Version: 7.0.22-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dialectsdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `analysis_items`
--

CREATE TABLE `analysis_items` (
  `id` int(11) NOT NULL,
  `dialect_entry_id` int(11) NOT NULL,
  `section` int(11) NOT NULL,
  `maintext` text NOT NULL,
  `examples` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dialect_entries`
--

CREATE TABLE `dialect_entries` (
  `id` int(11) NOT NULL,
  `dialect_key` varchar(50) DEFAULT NULL,
  `location_name` varchar(50) DEFAULT NULL,
  `location_label` varchar(10) DEFAULT NULL,
  `audio` varchar(255) DEFAULT NULL,
  `location_latitude` decimal(10,6) DEFAULT NULL,
  `location_longitude` decimal(10,6) DEFAULT NULL,
  `metadata` text NOT NULL,
  `transcription` text NOT NULL,
  `standard_slovene` text NOT NULL,
  `analysis1` text NOT NULL,
  `analysis2` text NOT NULL,
  `analysis3` text NOT NULL,
  `analysis4` text NOT NULL,
  `analysis5` text NOT NULL,
  `analysis6` text NOT NULL,
  `analysis7` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `is_admin` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name`, `is_admin`) VALUES
(1, 'urednik', '$2y$10$ZFS092RmBFTChyHYxMxcfu83VC4t4s0kU73mFQPjMW8Yk0S.nQBR6', 'Glavni Urednik', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `analysis_items`
--
ALTER TABLE `analysis_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `SECONDARY` (`dialect_entry_id`) USING BTREE;

--
-- Indexes for table `dialect_entries`
--
ALTER TABLE `dialect_entries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `analysis_items`
--
ALTER TABLE `analysis_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `dialect_entries`
--
ALTER TABLE `dialect_entries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `analysis_items`
--
ALTER TABLE `analysis_items`
  ADD CONSTRAINT `analysis_items_ibfk_1` FOREIGN KEY (`dialect_entry_id`) REFERENCES `dialect_entries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
